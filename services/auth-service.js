const User = require('../models/User');
const bcrypt = require('bcryptjs');
const tokenService = require('./token-service');
const ApiError = require('../exceptions/api-error');

class AuthService {
  async registration(role, email, password) {
    const candidate = await User.findOne({email});
    if (candidate) {
      throw ApiError.BadRequest(`User with ${email} already exists`);
    }
    const hashPassword = await bcrypt.hash(password, 7);
    const user = await User.create({
      role,
      email,
      password: hashPassword,
      created_date: new Date(),
    });
    const tokens = tokenService.generateTokens(user._id, user.role);
    await tokenService.saveToken(user._id, tokens.refreshToken);

    return {...tokens};
  }

  async login(email, password) {
    const user = await User.findOne({email});
    if (!user) {
      throw ApiError.BadRequest(`No user with ${email} email found`);
    }
    const isPassEquals = await bcrypt.compare(password, user.password);
    if (!isPassEquals) {
      throw ApiError.BadRequest('Wrong password');
    }
    const tokens = tokenService.generateTokens(user._id, user.role);

    await tokenService.saveToken(user._id, tokens.refreshToken);
    return {...tokens, user};
  }

  async logout(refreshToken) {
    const token = await tokenService.removeToken(refreshToken);
    return token;
  }

  async refresh(refreshToken) {
    if (!refreshToken) {
      throw ApiError.UnauthorizedError();
    }
    const userData = tokenService.validateRefreshToken(refreshToken);
    const tokenFromDb = await tokenService.findToken(refreshToken);
    if (!userData || !tokenFromDb) {
      throw ApiError.UnauthorizedError();
    }
    const user = await User.findById(userData.id);
    const tokens = tokenService.generateTokens(user._id, user.role);

    await tokenService.saveToken(user._id, tokens.refreshToken);
    return {...tokens};
  }

  async checkUserData(id, role) {
    const user = await User.findOne({_id: id});
    if (!user) {
      throw ApiError.BadRequest(`User not found`);
    }
    if (user.role !== role) {
      throw ApiError.BadRequest(`User is not a driver`);
    }
    return user;
  }
}

module.exports = new AuthService();
