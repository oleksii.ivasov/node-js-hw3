const {Schema, model} = require('mongoose');

const schema = new Schema(
    {
      created_by: {
        type: Schema.Types.ObjectId, ref: 'User',
        required: true,
      },
      assigned_to: {
        type: Schema.Types.ObjectId, ref: 'User',
        default: null,
      },
      type: {
        type: String,
        required: true,
        enum: ['SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT'],
        default: 'SPRINTER',
      },
      status: {
        type: String,
        required: true,
        enum: ['OL', 'IS'],
      },
      created_date: {
        type: Date,
        required: true,
      },
    },
    {versionKey: false},
);

module.exports = model('Truck', schema);
