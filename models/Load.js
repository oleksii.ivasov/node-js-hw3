const {Schema, model} = require('mongoose');

const schema = new Schema(
    {
      created_by: {type: Schema.Types.ObjectId, ref: 'User', required: true},
      assigned_to: {type: Schema.Types.ObjectId, ref: 'User', default: null},
      status: {
        type: String,
        enum: ['NEW', 'POSTED', 'ASSIGNED', 'SHIPPED'],
        required: true,
      },
      state: {
        type: String,
        enum: [
          'En route to Pick Up',
          'Arrived to Pick Up',
          'En route to delivery',
          'Arrived to delivery',
        ],
        required: true,
      },
      name: {
        type: String,
        required: true,
      },
      payload: {
        type: Number,
        required: true,
      },
      pickup_address: {
        type: String,
        required: true,
      },
      delivery_address: {
        type: String,
        required: true,
      },
      dimensions: {
        width: {
          type: Number,
          required: true,
        },
        length: {
          type: Number,
          required: true,
        },
        height: {
          type: Number,
          required: true,
        },
      },
      logs: [
        {
          message: {
            type: String,
            required: true,
          },
          time: {
            type: Date,
            required: true,
          },
        },
      ],
      created_date: {
        type: Date,
        required: true,
      },
    },
    {
      versionKey: false,
    },
);

module.exports = model('Load', schema);
