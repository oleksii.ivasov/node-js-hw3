const {Schema, model} = require('mongoose');

const schema = new Schema(
    {
      role: {
        type: String,
        required: true,
      },
      email: {
        type: String,
        required: true,
        unique: true,
      },
      password: {
        type: String,
        required: true,
      },
      created_date: {
        type: Date,
        required: true,
      },
    },
    {
      versionKey: false,
    },
);

module.exports = model('User', schema);
