const Joi = require('joi');

const schema = Joi.object({

  type: Joi.string()
      .pattern(/SPRINTER|SMALL STRAIGHT|LARGE STRAIGHT/)
      .required(),
});

module.exports = schema;
