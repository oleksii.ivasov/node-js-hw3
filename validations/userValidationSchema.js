const Joi = require('joi');

const schema = Joi.object({
  password: Joi.string().pattern(new RegExp('^[a-zA-Z0-9]{3,30}$')).required(),

  role: Joi.string()
      .pattern(/SHIPPER|DRIVER/)
      .required(),

  email: Joi.string()
      .email({
        minDomainSegments: 2,
        tlds: {allow: ['com', 'net']},
      })
      .required(),
});

module.exports = schema;
