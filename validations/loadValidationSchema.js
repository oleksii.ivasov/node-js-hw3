const Joi = require('joi');

const schema = Joi.object({
  name: Joi.string().required(),

  payload: Joi.number().required(),

  pickup_address: Joi.string().required(),

  delivery_address: Joi.string().required(),

  dimensions: Joi.object().required(),
});

module.exports = schema;
