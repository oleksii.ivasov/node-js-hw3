const Truck = require('../models/Truck');
const User = require('../models/User');
const authService = require('../services/auth-service');
const truckValidationSchema = require('../validations/truckValidationSchema');
const ApiError = require('../exceptions/api-error');

class TruckController {
  async assignTruckById(req, res, next) {
    try {
      if (!req.params.id) {
        res.json('No truck id')
      }
      const user = await authService.checkUserData(req.user.id, 'DRIVER');
      const assignedTruck = await Truck.findOne({assigned_to: user.id});
      if (assignedTruck) {
        throw ApiError.BadRequest('Driver already has an assigned truck');
      }
      const {id} = req.params;
      await Truck.updateOne({_id: id}, {$set: {assigned_to: user.id}});
      res.json({message: 'Truck assigned successfully'});
    } catch (e) {
      next(e);
    }
  }

  async deleteTruckById(req, res, next) {
    try {
      await authService.checkUserData(req.user.id, 'DRIVER');
      const {id} = req.params;
      await Truck.deleteOne({_id: id});
      res.json({message: 'Truck deleted successfully'});
    } catch (e) {
      next(e);
    }
  }

  async updateTruckById(req, res, next) {
    try {
      await authService.checkUserData(req.user.id, 'DRIVER');
      try {
        await truckValidationSchema.validateAsync(req.body);
      } catch (e) {
        throw ApiError.BadRequest(`Validation error: ${e.message}`);
      }
      const {type} = req.body;
      const {id} = req.params;
      await Truck.updateOne({_id: id}, {$set: {type}});
      res.json({message: 'Truck details changed successfully'});
    } catch (e) {
      next(e);
    }
  }

  async getTruckById(req, res, next) {
    try {
      await authService.checkUserData(req.user.id, 'DRIVER');
      const {id} = req.params;
      const truck = await Truck.findOne({_id: id});
      if (!truck) {
        throw ApiError.BadRequest('There is no truck with this id');
      }
      res.json({truck});
    } catch (e) {
      next(e);
    }
  }

  async addTruck(req, res, next) {
    try {
      const user = await User.findOne({_id: req.user.id});
      if (!user) {
        throw ApiError.BadRequest(`User not found`);
      }
      try {
        await truckValidationSchema.validateAsync(req.body);
      } catch (e) {
        throw ApiError.BadRequest(`Validation error: ${e.message}`);
      }
      if (user.role !== 'DRIVER') {
        throw ApiError.BadRequest(`User is not a driver`);
      }
      const {type} = req.body;
      await Truck.create({
        created_by: user._id,
        assigned_to: null,
        type,
        status: 'IS',
        created_date: new Date(),
      });
      res.json({message: 'Truck created successfully'});
    } catch (e) {
      next(e);
    }
  }

  async getTrucks(req, res, next) {
    try {
      const user = await authService.checkUserData(req.user.id, 'DRIVER');
      const trucks = await Truck.find({created_by: user.id});
      console.log(trucks);
      res.json({trucks: trucks});
    } catch (e) {
      next(e);
    }
  }
}

module.exports = new TruckController();
