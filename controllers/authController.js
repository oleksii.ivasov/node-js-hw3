const userValidationSchema = require('../validations/userValidationSchema');
const authService = require('../services/auth-service');
const ApiError = require('../exceptions/api-error');
require('dotenv').config();

class AuthController {
  async registration(req, res, next) {
    try {
      const {email, password, role} = req.body;
      const {refreshToken} = req.cookies;
      if (refreshToken) {
        await authService.logout(refreshToken);
        res.clearCookie('refreshToken');
      }
      try {
        await userValidationSchema.validateAsync(req.body);
      } catch (e) {
        throw ApiError.BadRequest(`Validation error: ${e.message}`);
      }
      const tokens = await authService.registration(role, email, password);
      res.cookie('refreshToken', tokens.refreshToken, {
        maxAge: 30 * 24 * 60 * 60 * 1000,
        httpOnly: true,
      });
      return res.json({message: 'Profile created successfully'});
    } catch (e) {
      next(e);
    }
  }

  async login(req, res, next) {
    try {
      const {email, password} = req.body;
      const userData = await authService.login(email, password);
      res.cookie('refreshToken', userData.refreshToken, {
        maxAge: 30 * 24 * 60 * 60 * 1000,
        httpOnly: true,
      });
      return res.json({jwt_token: userData.accessToken});
    } catch (e) {
      next(e);
    }
  }

  async forgotPassword(req, res, next) {
    try {
      // const {userEmail} = req.body;
      // const transporter = nodemailer.createTransport({
      //   host: process.env.SMTP_HOST,
      //   port: process.env.SMTP_PORT,
      //   secure: false,
      //   auth: {
      //     user: process.env.SMTP_USER,
      //     pass: process.env.SMTP_PASSWORD,
      //   },
      // });
      // await transporter.sendMail({
      //   from: process.env.SMTP_USER,
      //   to: userEmail,
      //   subject: `Forget password on ${process.env.API_URL}`,
      //   text: '',
      //   html: `<div>
      //   <h1>Your new password: 'mynewpass'</h1>
      //   If it's not you just ignore this message
      //   </div>`,
      // })
      res.json({message: 'New password sent to your email address'});
    } catch (e) {
      next(e);
    }
  }

  async refresh(req, res, next) {
    try {
      const {refreshToken} = req.cookies;
      const tokens = await authService.refresh(refreshToken);
      res.cookie('refreshToken', tokens.refreshToken, {
        maxAge: 30 * 24 * 60 * 60 * 1000,
        httpOnly: true,
      });
      res.json({tokens});
    } catch (e) {
      next(e);
    }
  }

  async logout(req, res, next) {
    try {
      const {refreshToken} = req.cookies;
      if (!refreshToken) {
        throw ApiError.BadRequest(`User is not logged in`);
      }
      await authService.logout(refreshToken);
      res.clearCookie('refreshToken');
      return res.json({message: `User logged out`});
    } catch (e) {
      next(e);
    }
  }
}

module.exports = new AuthController();
