const Truck = require('../models/Truck');
const User = require('../models/User');
const Load = require('../models/Load');
const loadValidationSchema = require('../validations/loadValidationSchema');
const authService = require('../services/auth-service');
const ApiError = require('../exceptions/api-error');

class LoadController {
  async addLoad(req, res, next) {
    try {
      const user = await authService.checkUserData(req.user.id, 'SHIPPER');
      try {
        await loadValidationSchema.validateAsync(req.body);
      } catch (e) {
        throw ApiError.BadRequest(`Validation error: ${e.message}`);
      }
      const {name, payload, pickup_address, delivery_address, dimensions} =
        req.body;
      await Load.create({
        created_by: user.id,
        status: 'NEW',
        state: 'En route to Pick Up',
        name,
        payload,
        pickup_address,
        delivery_address,
        dimensions,
        created_date: new Date(),
      });
      res.json({message: 'Load created successfully'});
    } catch (e) {
      next(e);
    }
  }

  async getAciveLoad(req, res, next) {
    try {
      const user = await authService.checkUserData(req.user.id, 'DRIVER');
      const load = await Load.findOne({assigned_to: user._id});
      res.json({message: load});
    } catch (e) {
      next(e);
    }
  }

  async iterateLoadState(req, res, next) {
    try {
      const user = await authService.checkUserData(req.user.id, 'DRIVER');
      const load = await Load.findOne({assigned_to: user.id});
      const states = [
        'En route to Pick Up',
        'Arrived to Pick Up',
        'En route to delivery',
        'Arrived to delivery',
      ];
      let newStateIdx = states.findIndex((s) => load.state === s) + 1;
      if (newStateIdx >= 4) {
        newStateIdx = 0;
      }
      const newState = states[newStateIdx];
      load.state = newState;
      load.save();
      res.json({message: `Load state changed to '${newState}'`});
    } catch (e) {
      next(e);
    }
  }

  async postLoad(req, res, next) {
    try {
      await authService.checkUserData(req.user.id, 'SHIPPER');
      const {id} = req.params;
      const load = await Load.findOne({_id: id});
      if (load.assigned_to) {
        throw ApiError.BadRequest('Load is already assigned');
      }
      const trucks = await Truck.find({status: 'IS'});
      const truckPayloads = {
        SPRINTER: {width: 300, length: 250, height: 170, payload: 1700},
        SMALL_STRAIGHT: {width: 500, length: 250, height: 170, payload: 2500},
        LARGE_STRAIGHT: {width: 700, length: 350, height: 200, payload: 4000},
      };
      let truckType;
      let driver_found = false;
      for (const truck of trucks) {
        truckType = Object.keys(truckPayloads).find(
            (el) => el === truck.type.replace(' ', '_'),
        );
        if (
          truckPayloads[truckType].width < load.dimensions.width ||
          truckPayloads[truckType].length < load.dimensions.length ||
          truckPayloads[truckType].height < load.dimensions.height ||
          truckPayloads[truckType].payload < load.payload
        ) {
          continue;
        } else {
          load.assigned_to = truck.assigned_to;
          driver_found = true;
          truck.status = 'OL';
          await truck.save();
          break;
        }
      }
      load.status = 'POSTED';
      await load.save();
      if (driver_found) {
        load.status = 'ASSIGNED';
      } else {
        load.status = 'NEW';
      }
      await load.save();
      res.json({message: 'Load posted successfully', driver_found});
    } catch (e) {
      next(e);
    }
  }

  async getLoad(req, res, next) {
    try {
      const {id} = req.params;
      const load = await Load.findOne({_id: id});
      if (!load) {
        throw ApiError('No load with this id');
      }
      res.json({load});
    } catch (e) {
      next(e);
    }
  }

  async getLoads(req, res, next) {
    try {
      const user = await User.findOne({_id: req.user.id});
      let {status, limit, offset} = req.query;
      if (user.role === 'DRIVER') {
        if (!status) {
          status = 'ASSIGNED';
        }
        const loads = await Load.find({status: status})
            .skip(offset)
            .limit(limit);
        res.json({loads});
      } else {
        if (status) {
          const loads = await Load.find({status: status})
              .skip(offset)
              .limit(limit);
          res.json({loads});
        } else {
          const loads = await Load.find({}).skip(offset).limit(limit);
          res.json({loads});
        }
      }
    } catch (e) {
      next(e);
    }
  }

  async getUsersLoadById(req, res, next) {
    try {
      await authService.checkUserData(req.user.id, 'SHIPPER');
      const {id} = req.params;
      const load = await Load.findOne({_id: id});
      const truck = await Truck.findOne({assigned_to: load.assigned_to});
      if (!load) {
        throw ApiError.BadRequest('No load with this id');
      }
      if (!truck) {
        throw ApiError.BadRequest('No truck assigned for this load');
      }
      res.json({load, truck});
    } catch (e) {
      next(e);
    }
  }

  async updateUsersLoadById(req, res, next) {
    try {
      await authService.checkUserData(req.user.id, 'SHIPPER');
      const {id} = req.params;
      const load = await Load.findOne({_id: id});
      const {name, payload, pickup_address, delivery_address, dimensions} =
        req.body;
      if (!load) {
        throw ApiError('No load with this id');
      }
      await Load.updateOne(
          {_id: id},
          {
            $set: {name, payload, pickup_address, delivery_address, dimensions},
          },
      );
      res.json({message: 'Load details changed successfully'});
    } catch (e) {
      next(e);
    }
  }

  async deleteLoadById(req, res, next) {
    try {
      await authService.checkUserData(req.user.id, 'SHIPPER');
      const {id} = req.params;
      await Load.deleteOne({_id: id});
      res.json({message: 'Load deleted successfully'});
    } catch (e) {
      next(e);
    }
  }
}

module.exports = new LoadController();
