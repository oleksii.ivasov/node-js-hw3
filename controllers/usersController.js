const bcrypt = require('bcryptjs/dist/bcrypt');
const User = require('../models/User');
const ApiError = require('../exceptions/api-error');
const authService = require('../services/auth-service');
const changePasswordValidationSchema = require('../validations/changePasswordValidationSchema');

class UsersController {
  async getUser(req, res, next) {
    try {
      const user = await User.findOne({_id: req.user.id});
      if (!user) {
        throw ApiError.BadRequest(`User not found`);
      }
      user.password = undefined;
      return res.json({user: user});
    } catch (e) {
      next(e);
    }
  }
  async deleteUser(req, res, next) {
    try {
      const user = await User.findOne({_id: req.user.id});
      if (!user) {
        throw ApiError.BadRequest(`User not found`);
      }
      const {refreshToken} = req.cookies;
      if (refreshToken) {
        await authService.logout(refreshToken);
        res.clearCookie('refreshToken');
      }
      await User.deleteOne({_id: user._id});
      return res.json({message: 'Success'});
    } catch (e) {
      next(e);
    }
  }
  async changePassword(req, res, next) {
    try {
      const {oldPassword, newPassword} = req.body;
      try {
        await changePasswordValidationSchema.validateAsync(req.body);
      } catch (e) {
        throw ApiError.BadRequest(`Validation error: ${e.message}`);
      }
      const user = await User.findOne({_id: req.user.id});
      if (!user) {
        throw ApiError.BadRequest(`User not found`);
      }
      const validPassword = await bcrypt.compare(oldPassword, user.password);
      if (!validPassword) {
        throw ApiError.BadRequest(`Wrong password`);
      }
      const hashPassword = bcrypt.hashSync(newPassword, 7);
      await User.findOneAndUpdate(
          {_id: user._id},
          {password: hashPassword},
      );
      return res.json({message: `Success`});
    } catch (e) {
      next(e);
    }
  }
}

module.exports = new UsersController();
