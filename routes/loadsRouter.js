const {Router} = require('express');
const checkAuth = require('../middleware/check-auth');
const router = Router();
const controller = require('../controllers/loadController');

router.post('/', checkAuth, controller.addLoad);

router.get('/active', checkAuth, controller.getAciveLoad);

router.patch('/active/state', checkAuth, controller.iterateLoadState);

router.post('/:id/post', checkAuth, controller.postLoad);

router.get('/:id', checkAuth, controller.getLoad);

router.get('/', checkAuth, controller.getLoads);

router.get('/:id/shipping_info', checkAuth, controller.getUsersLoadById);

router.put('/:id', checkAuth, controller.updateUsersLoadById);

router.delete('/:id', checkAuth, controller.deleteLoadById);

module.exports = router;
