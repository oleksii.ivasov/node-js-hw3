const {Router} = require('express');

const router = Router();
const controller = require('../controllers/authController');

router.post('/register', controller.registration);
router.post('/login', controller.login);

router.post('/logout', controller.logout);

router.post('/forgot_password', controller.forgotPassword);

router.get('/refresh', controller.refresh);

module.exports = router;
