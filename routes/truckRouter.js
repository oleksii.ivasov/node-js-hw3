const { Router } = require('express');
const controller = require('../controllers/truckContoller');
const checkAuth = require('../middleware/check-auth');

const router = Router();

router.post('/assign', checkAuth, controller.assignTruckById);

router.get('/', checkAuth, controller.getTrucks);

router.get('/:id', checkAuth, controller.getTruckById);

router.post('/', checkAuth, controller.addTruck);

router.put('/:id', checkAuth, controller.updateTruckById);

router.delete('/:id', checkAuth, controller.deleteTruckById);

router.post('/:id/assign', checkAuth, controller.assignTruckById);

module.exports = router;
