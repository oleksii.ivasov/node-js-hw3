const {Router} = require('express');
const controller = require('../controllers/usersController');
const checkAuth = require('../middleware/check-auth');

const router = Router();

router.get('/me', checkAuth, controller.getUser);

router.delete('/me', checkAuth, controller.deleteUser);

router.patch('/me/password', checkAuth, controller.changePassword);

module.exports = router;
