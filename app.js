const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const cookieParser = require('cookie-parser');
require('dotenv').config();
const authRouter = require('./routes/authRouter');
const usersRouter = require('./routes/usersRouter');
const truckRouter = require('./routes/truckRouter');
const loadsRouter = require('./routes/loadsRouter');
const errorMiddleware = require('./middleware/error-middleware');
const PORT = process.env.PORT || 8080;

const app = express();

app.use(express.json());
app.use(cors());
app.use(cookieParser());
app.use(express.urlencoded({extended: true}));
app.use('/api/users', usersRouter);
app.use('/api/auth', authRouter);
app.use('/api/trucks', truckRouter);
app.use('/api/loads', loadsRouter);
app.use(errorMiddleware);

const start = async () => {
  try {
    await mongoose.connect(process.env.DB_URL, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    app.listen(PORT, () => {
      console.log(`Server started on port ${PORT}`);
    });
  } catch (e) {
    console.log(e);
  }
};

start();
